
var FormView = BaseView.extend({

    events:{
        "click button" : "saveUser"
    },

    saveUser: function(event){
        event.preventDefault();
        
        this.model.set({
            "name": this.$('.user-name').val(),
            "email": this.$('.user-email').val(),
        })
    }
})