

var Users = Backbone.Collection.extend({
    model: User,
    url: 'http://localhost:3000/users/'
})

var users = new Users([ ])

UsersView = Backbone.View.extend({

    initialize: function (options) {
        this.collection = options.collection;
        this.selected = options.selected;

        this.listenTo(this.collection,'all', this.render)
        this.listenTo(this.selected,'all', this.render)

        // this.listenTo(this.selected,'change', function(model){
        //     this.$('.list-group-item')
        //         .removeClass('active')
        //         .filter('[data-id='+model.get('id')+']')
        //         .addClass('active')
        // })

        this.render()
    },

    events:{
        "click .list-group-item":"selectUser"
    },

    selectUser:function(event){
        var id = $(event.target).data('id')
        this.selected.set( this.collection.get(id).toJSON() )
    },

    render: function () {
        var liTpl = _.template('<li data-id="<%= id %>"  '
        +' class="list-group-item <%= selected? \'active\':\'\'  %>"> <%= name %>,  <%= email %> </li>')

        var list = this.collection.toJSON().map(function (user) {
            return liTpl(_.extend(user,{selected: user.id == this.selected.get('id')}))
        })
        this.$el.html('<ul class="list-group">' + list.join('') + '</ul>')
    }
})

var selected = new User({
    name:'', email: ''
})

var usersView = new UsersView({
    el: $('#users-view'),
    collection: users,
    selected: selected
})

users.fetch()

var userForm = new FormView({
    el: $('#user-form'),
    model: selected,

    template: _.template(
        $('#user-form-template').text()
    ),
})