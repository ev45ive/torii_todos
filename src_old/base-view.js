var BaseView = Backbone.View.extend({
    initialize:function(options){
        this.template = options.template;
        this.listenTo(this.model, 'all', this.render)

        this.render()
    },

    render: function(){
        this.$el.html( this.template(this.model.toJSON()))
    }
})