var Todo = Backbone.Model.extend({
    defaults: {
        title: '',
        completed: false
    }
})

var TodoView = Backbone.View.extend({
    tagName: 'li',
    className: 'list-group-item',
    attributes: function(){ return { 
        'data-id': this.model.get('id')
    }},

    initialize: function(){
        this.render()
        this.listenTo( this.model, "change", this.render)
        this.listenTo( this.model, "sync", this.render)
    },

    events:{
        'change input[type="checkbox"]': "toggleCompleted",
        //'keyup input[type="text"]': "changeTitle"
        'dblclick .todo-title':'toggleEditing',
        'blur input':'toggleEditing',
        'keyup input':'changeTitle',
        'click .todo-remove': 'destroyTodo'
    },

    destroyTodo:function(){
        this.model.destroy();
    },

    changeTitle: function(event){
        if(event.keyCode == 13){
            this.editing = false;
            this.model.set('title', event.target.value)
            this.model.save()
        }
    },

    toggleCompleted: function(event){
        var previous = this.model.get('completed')
        this.model.set('completed', !previous)
        this.model.save()
    },

    template: _.template( $('#todo-template').text() ),

    toggleEditing: function(){
        this.editing = !this.editing;
        this.render()
    },

    render: function () {
        this.$el.html( this.template({
            todo: this.model.toJSON(),
            editing: this.editing
        }))
    }
})

/////////////////////////////////////

var Todos = Backbone.Collection.extend({
    url:'http://localhost:3000/todos/',
    model: Todo
})

todos = new Todos([
    // {
    //     id: '1',
    //     title: 'Testowy'
    // },
])

var TodosView = Backbone.View.extend({

    tagName: 'ul',
    className: 'list-group todos-view',

    initialize:function(){
        this.render()
        this.listenTo(this.collection,'add',this.onAdd)
        this.listenTo(this.collection,'remove',this.onRemove)
    },

    onAdd:function(model){
        var view = new TodoView({ model: model })
        view.$el.data('view',view)
        this.$el.append(view.el)
    },
    onRemove:function(model){
        var id = model.get('id')
        this.$('[data-id='+id+']').data('view').remove()
    },

    render: function(){
        // this.$el.empty()
        // var todos = this.collection.map(function(todo, index){
        //     var view = new TodoView({ model: todo })
        //     return view.el
        // })
        // this.$el.append(todos)
    }
})

view = new TodosView({
    collection: todos
})

$('#todos-view').append(view.el)

var StatsView = Backbone.View.extend({

    initialize:function(){
        this.render()
        this.listenTo(this.collection,'update change:completed',this.render)
    },

    render:function(){
        var stats = this.collection.countBy(function(model){
            return model.get('completed')? 'completed':'notcompleted'
        })
        this.$el.html('Completed: '+stats.completed)
    } 
})
stats = new StatsView({
    collection: todos
})

$('#todos-view').append(stats.el)


var TodoForm = Backbone.View.extend({
    initialize: function(){
        this.render()
    },
    events:{
        'keyup input':'onChange'
    },

    onChange:function(event){
        if(event.keyCode == 13){
            this.collection.push({
                title: event.target.value
            });
            event.target.value = ''
        }
    },

    render: function(){
        this.$el.html('<input class="form-control todo-title">')
    }
})
form = new TodoForm({
    collection: todos
})
$('#todos-view').append(form.el)

todos.fetch({
    data:{
        userId:1,
        //_sort: 'completed',
        // _page:1, 
        _limit:3
    }
})